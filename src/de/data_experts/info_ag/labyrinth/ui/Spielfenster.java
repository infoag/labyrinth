package de.data_experts.info_ag.labyrinth.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import de.data_experts.info_ag.labyrinth.model.Spielbrett;

public class Spielfenster {

    private final Spielbrett spielbrett;
    
    private SpielbrettPanel spielbrettPanel = null;

    private JFrame frame = new JFrame("Labyrinth");

    public Spielfenster(Spielbrett spielbrett) {
        this.spielbrett = spielbrett;
        initUi();
    }

    private void initUi() {

        frame.setSize(800, 800);

        frame.setJMenuBar(createMenuBar());

        spielbrettPanel = new SpielbrettPanel(spielbrett);
        frame.getContentPane().add(spielbrettPanel);
        //        frame.setUndecorated(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100, 100);
        frame.setVisible(true);
    }

    private void saveGame() {
        try {
            JFileChooser chooser = new JFileChooser();
            int choice = chooser.showOpenDialog(frame);
            if (choice != JFileChooser.APPROVE_OPTION) {
                return;
            }
            Files.write(chooser.getSelectedFile().toPath(), spielbrett.getTextRepresentation().getBytes());
            JOptionPane.showMessageDialog(frame, "Spiel gespeichert");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(frame, "Das Spiel konnte nicht gespeichert werden :(");
        }
    }
    
    private void loadGame(){
        try {
            JFileChooser chooser = new JFileChooser();
            int choice = chooser.showOpenDialog(frame);
            if (choice != JFileChooser.APPROVE_OPTION) {
                return;
            }
            String spielStand = new String(Files.readAllBytes(chooser.getSelectedFile().toPath()));
            spielbrett.loadFromTextRepresentation(spielStand);
            spielbrettPanel.repaint();
            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Das Spiel konnte nicht geladen werden :(");
        }
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("Datei");
        JMenuItem saveMenu = new JMenuItem("Speichern");
        saveMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                saveGame();
            }
        });
        fileMenu.add(saveMenu);

        JMenuItem loadMenu = new JMenuItem("Laden");
        loadMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               loadGame();
            }
        });
        fileMenu.add(loadMenu);
        menuBar.add(fileMenu);

        return menuBar;
    }

}
