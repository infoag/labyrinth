package de.data_experts.info_ag.labyrinth.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import de.data_experts.info_ag.labyrinth.model.Feld;
import de.data_experts.info_ag.labyrinth.model.Feldbelegung;
import de.data_experts.info_ag.labyrinth.model.Spielbrett;

/**
 * GUI-Komponente, die unser Spielbrett rendered.
 */
@SuppressWarnings("serial")
public class SpielbrettPanel extends JPanel {

    private Spielbrett spielbrett;

    private int erlaubteAnzahlBlocker;

    private final int abstandLinks = 30; // linker Abstand des gesamten Rasters innerhalb des Panels vom Rand
    private int abstandRasterOben = 50; // oberer Abstand des gesamten Rasters innerhalb des Panels vom Rand
    private final int kantenLaenge = 40;
    private final int pixelAbstandVomRand = 5;

    private MouseAdapter spielBrettEditHandler = new MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent e) {
            int spalteGeklickt = (e.getX() - abstandLinks) / kantenLaenge;
            int zeileGeklickt = (e.getY() - abstandRasterOben) / kantenLaenge;

            Feldbelegung belegungVorKlick = spielbrett.getFeld(zeileGeklickt, spalteGeklickt).getBelegung();
            Feldbelegung belegungNachKlick = null;

            if (spielbrett.isErsteller()) {
                if (belegungVorKlick != Feldbelegung.FREI) {
                    belegungNachKlick = Feldbelegung.FREI;
                } else {
                    // es ist noch kein Stein hier
                    if (spielbrett.getAnzahlSteine(Feldbelegung.START) < 1) {
                        belegungNachKlick = Feldbelegung.START;
                    } else if (spielbrett.getAnzahlSteine(Feldbelegung.ZIEL) < 1) {
                        belegungNachKlick = Feldbelegung.ZIEL;
                    } else {
                        int aktuelleZahlBlocker = spielbrett.getAnzahlSteine(Feldbelegung.BLOCKIERT);
                        if (aktuelleZahlBlocker < erlaubteAnzahlBlocker) {
                            belegungNachKlick = Feldbelegung.BLOCKIERT;
                        } else {
                            belegungNachKlick = Feldbelegung.FREI;
                        }
                    }
                }

                spielbrett.getFeld(zeileGeklickt, spalteGeklickt).setBelegung(belegungNachKlick);
            } else {
                // kein Ersteller -> nur Feld aufdecken
                spielbrett.getFeld(zeileGeklickt, spalteGeklickt).setVerdeckt(false);
                spielbrett.listAufgedeckteWege();
                
                if (spielbrett.isGeloest()) {
                    JOptionPane.showMessageDialog(SpielbrettPanel.this, "Gl�ckwunsch!!!");
                }
            }
            SpielbrettPanel.this.repaint();
        };

    };

    public SpielbrettPanel(Spielbrett spielbrett) {
        this.spielbrett = spielbrett;

        int anzahlFelder = spielbrett.getAnzahlSpalten() * spielbrett.getAnzahlZeilen();
        erlaubteAnzahlBlocker = anzahlFelder / 5;

        addMouseListener(spielBrettEditHandler);
    }

    @Override
    public void paint(Graphics g) {
        paintRaster(g);
        paintSteine(g);
    }

    /**
     * Zeichnet die Gitternetzlinien f�r unser Spielbrett
     */
    private void paintRaster(Graphics g) {
        g.setColor(Color.black);

        int posX = abstandLinks;
        int posY = abstandRasterOben;

        for (int zeile = 0; zeile < spielbrett.getAnzahlZeilen(); zeile++) {
            for (int spalte = 0; spalte < spielbrett.getAnzahlSpalten(); spalte++) {
                g.drawRect(posX, posY, kantenLaenge, kantenLaenge);
                posX += kantenLaenge;
            }

            posY += kantenLaenge;
            posX = abstandLinks;
        }
    }

    /**
     * Zeichnet die Steine f�r unser Spielbrett
     */
    private void paintSteine(Graphics g) {
        int posX = abstandLinks;
        int posY = abstandRasterOben;

        for (int zeile = 0; zeile < spielbrett.getAnzahlZeilen(); zeile++) {
            for (int spalte = 0; spalte < spielbrett.getAnzahlSpalten(); spalte++) {
                Feld feld = spielbrett.getFeld(zeile, spalte);
                if (!spielbrett.isErsteller() && feld.getBelegung() != Feldbelegung.START && feld.getBelegung() != Feldbelegung.ZIEL && feld.isVerdeckt()) {
                    g.setColor(Color.ORANGE);
                } else {
                    if (feld.getBelegung() == Feldbelegung.BLOCKIERT) {
                        g.setColor(Color.black);
                    } else if (feld.getBelegung() == Feldbelegung.START) {
                        g.setColor(Color.blue);
                    } else if (feld.getBelegung() == Feldbelegung.ZIEL) {
                        g.setColor(Color.red);
                    } else if (feld.getBelegung() == Feldbelegung.FREI) {
                        g.setColor(getBackground());
                    }
                }

                g.fillOval(posX + pixelAbstandVomRand, posY + pixelAbstandVomRand, kantenLaenge - 2 * pixelAbstandVomRand, kantenLaenge - 2 * pixelAbstandVomRand);

                posX += kantenLaenge;
            }

            posY += kantenLaenge;
            posX = abstandLinks;
        }
    }
}
