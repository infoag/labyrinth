package de.data_experts.info_ag.labyrinth.model;

/**
 * Einzelnes Feld auf dem Spielbrett, auf das ein Stein (Blocker oder Startstein oder Zielstein) gesetzt werden kann. 
 */
public class Feld {

    int spalte;
    
    int zeile;
    
    /**
     * Aktuelle Belegung des Feldes auf dem Spielbrett
     */
    private Feldbelegung belegung = Feldbelegung.FREI;
    
    /**
     * Gibt an, ob dieses Feld vom Spieler schon erraten bzw. geraten wurde
     */
    private boolean verdeckt = true;
    
    public Feld (int spalte, int zeile) {
        this.spalte = spalte;
        this.zeile = zeile;
    }
    /**
     * �ndert die Belegung des Feldes ("setzt einen Stein auf das Feld")
     * @param neueBelegung Stein, der auf das Feld gesetzt werden sollen
     */
    public void setBelegung (Feldbelegung neueBelegung)  {
        this.belegung = neueBelegung;
    }
    
    
    /**
     * Deckt ein Feld auf bzw. zu
     * @param verdeckt Angabe, ob das Feld zugedeckt sein soll
     */
    public void setVerdeckt (boolean verdeckt)  {
        this.verdeckt = verdeckt;
    }

    /**
     * Liefert den Stein, der auf dem Feld aktuell liegt
     * @return
     */
    public Feldbelegung getBelegung() {
        return belegung;
    }

    /**
     * Liefert die Angabe, ob das Feld aktuell verdeckt ist
     * @return
     */
    public boolean isVerdeckt() {
        return verdeckt;
    }
    
    @Override
    public String toString() {
        return spalte + "-" + zeile;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof Feld)) {
            return false;
        }
        
        Feld anderesFeld = (Feld) obj;
        
        return this.spalte == anderesFeld.spalte && this.zeile == anderesFeld.zeile;
    }
}
