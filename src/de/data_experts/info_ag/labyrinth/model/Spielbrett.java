package de.data_experts.info_ag.labyrinth.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Kombination aus nxn Spielfeldern, die das verf�gbare Spielbrett darstellen
 */
public class Spielbrett {

    /**
     * Matrix f�r die Zeilen und Spalten unseres Spielbretts.<br>
     * Der erste Werte steht f�r die Zeile, der zweite Wert steht f�r die Spalte
     */
    private final Feld[][] felder;
    
    private boolean ersteller = false;

    public Spielbrett(int anzahlZeilen) {
        felder = new Feld[anzahlZeilen][anzahlZeilen];

        for (int zeile = 0; zeile < felder.length; zeile++) {
            felder[zeile] = new Feld[anzahlZeilen];
            for (int spalte = 0; spalte < felder[zeile].length; spalte++) {
                felder[zeile][spalte] = new Feld(spalte,zeile);
            }
        }
    }

    public boolean isGeloest() {
        List<Feld> aufgedeckteWege = listAufgedeckteWege();
        List<Feld> nachbarnVomZiel = getNachbarfelder(getFeld(Feldbelegung.ZIEL));
        
        for (Feld nachbarVomZiel : nachbarnVomZiel) {
            if (aufgedeckteWege.contains(nachbarVomZiel)) {
                return true;
            }
        }
        
        return false;
    }
    
    public List<Feld> listAufgedeckteWege() {
        Feld start = getFeld(Feldbelegung.START);
        Feld letzterStartstein = null;

        List<Feld> aktuellerWeg = new ArrayList<>();
    //    aktuellerWeg.add(start);
        
        // in alle 4 Richtungen schauen, ob frei und aufgedeckt ist
        while (!start.equals(letzterStartstein)) {
            aktuellerWeg = getNextWeg (start , aktuellerWeg);
            letzterStartstein = start;
            start = aktuellerWeg.get(aktuellerWeg.size()-1);
        }
     
        
        printWegListe(aktuellerWeg);
        return aktuellerWeg;
    }
    
    public List<Feld> getNextWeg (Feld start, List<Feld> weg) {
        for (Feld nachbar : getNachbarfelder(start)) {
            if (!nachbar.isVerdeckt() && nachbar.getBelegung() == Feldbelegung.FREI && !weg.contains(nachbar)) {
                weg.add(nachbar);
          //      return weg;
//                start = nachbar;
                
               // System.out.println("start: " + start);
            }
        }
        
        return weg;
    //    return aktuellerWeg;
    }
    
    public void printWegListe (List<Feld> weg) {
        String s = "";
        for (Feld f : weg) {
            s += f + ";";
        }
        
        System.out.println(s);
    }
    
    /**
     * Liefert alle Nachbarfelder (links, oben, unten, rechts), die nicht null sind.
     * @param f
     * @return
     */
    public List<Feld> getNachbarfelder (Feld f) {
        List<Feld> result = new ArrayList<>();
        Feld nachbar = getNachbarfeld("rechts", f);
        if (nachbar != null) {
            result.add(nachbar);
        }
        nachbar = getNachbarfeld("links", f);
        if (nachbar != null) {
            result.add(nachbar);
        }
        nachbar = getNachbarfeld("oben", f);
        if (nachbar != null) {
            result.add(nachbar);
        }
        nachbar = getNachbarfeld("unten", f);
        if (nachbar != null) {
            result.add(nachbar);
        }
        
        return result;
    }
    
    
    public Feld getNachbarfeld (String richtung, Feld f) {
        int nachbarSpalte = 0;
        int nachbarZeile = 0;
        
        if ("links".equals(richtung)) {
            nachbarSpalte = f.spalte - 1;
            nachbarZeile = f.zeile;
        } else if ("rechts".equals(richtung)) {
            nachbarSpalte = f.spalte + 1;
            nachbarZeile = f.zeile;
        } else if ("unten".equals(richtung)) {
            nachbarSpalte = f.spalte;
            nachbarZeile = f.zeile + 1;
        }else if ("oben".equals(richtung)) {
            nachbarSpalte = f.spalte;
            nachbarZeile = f.zeile - 1;
        }
        
        if (nachbarSpalte < 0 || nachbarZeile < 0) {
            // Feld f hat in dieser Richtung keinen Nachbarn
            return null;
        }
        
        return getFeld(nachbarZeile, nachbarSpalte);
    }
    /**
     * Liefert das Feld, das in der entsprechenden Zeile und Spalte steht.
     * 
     * @param zeile
     * @param spalte
     * @return
     */
    public Feld getFeld(int zeile, int spalte) {
        return felder[zeile][spalte];
    }

    /**
     * Liefert den aktuellen Zustannd des Spielbretts und aller seiner Felder als String-Reprens�ntation.
     * 
     * @return Text, der das Spielfeld darstellt
     */
    public String getTextRepresentation() {
        String result = "";
        for (int zeile = 0; zeile < felder.length; zeile++) {
            for (int spalte = 0; spalte < felder[zeile].length; spalte++) {
                Feld feld = getFeld(zeile, spalte);
                result += feld.getBelegung() + ";";
            }
            result += "\n";
        }

        return result;
    }
    
    public void loadFromTextRepresentation (String textRepresentation) {
        System.out.println("Lade das Spielfeld....");
        int zeileNr = 0; 
        int spalteNr = 0;
        
        for (String zeile : textRepresentation.split("\\Q\n\\E")) {
            for (String zelle : zeile.split("\\Q;\\E")) {
                getFeld(zeileNr, spalteNr).setBelegung(Feldbelegung.valueOf(zelle));
                getFeld(zeileNr, spalteNr).setVerdeckt(true);
                spalteNr = spalteNr + 1;
            }
            
            zeileNr = zeileNr + 1;
            spalteNr = 0;
        }
    }
    
    /**
     * Liefert die Anzahl der Steine auf dem Spielbrett.
     * @return Anzahl der Steine (Startstein + Zielstein + Blocker)
     */
    public int getAnzahlSteine (Feldbelegung typ) {
        int anzahlGesamt = 0;
        for (int zeile = 0; zeile < getAnzahlZeilen(); zeile++) {
            for (int spalte = 0; spalte < getAnzahlSpalten(); spalte++) {
                if (getFeld(zeile, spalte).getBelegung() == typ) {
                    anzahlGesamt++;
                }
            }
        }
        
        return anzahlGesamt;
    }
    
    
    
    /**
     * Liefert die Anzahl der Zeilen, die f�r das Spielbrett definiert sind
     * @return  Anzahl der Zeilen, die f�r das Spielbrett definiert sind
     */
    public int getAnzahlZeilen() {
        return felder.length;
    }
    
    /**
     * Liefert die Anzahl der Spalten, die f�r das Spielbrett definiert sind
     *  @return  Anzahl der Zeilen, die f�r das Spielbrett definiert sind
     */ 
    public int getAnzahlSpalten() {
        return felder[0].length;
    }

    @Override
    public String toString() {
        return getTextRepresentation();
    }

    public void setErsteller(boolean ersteller) {
        this.ersteller = ersteller;
    }
    
    public boolean isErsteller() {
        return ersteller;
    }
    
    public Feld getFeld (Feldbelegung typ) {
        for (int zeile = 0; zeile < getAnzahlZeilen(); zeile++) {
            for (int spalte = 0; spalte < getAnzahlSpalten(); spalte++) {
                if (getFeld(zeile, spalte).getBelegung() == typ) {
                    return getFeld(zeile, spalte);
                }
            }
        }
        
        return null;
    }
}
