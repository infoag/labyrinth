package de.data_experts.info_ag.labyrinth.model;

/**
 * M�gliche Belegungen eines Spielfeldes
 */
public enum Feldbelegung {
    FREI, BLOCKIERT, START, ZIEL;
}
