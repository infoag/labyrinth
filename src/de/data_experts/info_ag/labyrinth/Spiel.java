package de.data_experts.info_ag.labyrinth;

import java.io.File;
import java.nio.file.Files;

import de.data_experts.info_ag.labyrinth.model.Feldbelegung;
import de.data_experts.info_ag.labyrinth.model.Spielbrett;
import de.data_experts.info_ag.labyrinth.ui.Spielfenster;

public class Spiel {

    private static final int ANZAHL_SPALTEN = 10;
    
    public static void main(String[] args) throws Exception {
        float kommaZahl = 2.875f;
        int ganzZahl = (int) kommaZahl;
        
        
        Spielbrett spielbrett = new Spielbrett(ANZAHL_SPALTEN);
        
        if (args.length > 0 && "ersteller".equalsIgnoreCase(args[0])) {
            spielbrett.setErsteller(true);
        } else {
            String spielStand = new String(Files.readAllBytes(new File("c:/Users/sbicher/Documents/20_mal_20").toPath()));
            spielbrett.loadFromTextRepresentation(spielStand);
        }
        
        
        
//        spielbrett.getFeld(0,0).setBelegung(Feldbelegung.START);
//        spielbrett.getFeld(7,7).setBelegung(Feldbelegung.ZIEL);
     //   spielbrett.getFeld(3,4).setBelegung(Feldbelegung.BLOCKIERT);
//        spielbrett.getFeld(1,1).setBelegung(Feldbelegung.BLOCKIERT);
//        spielbrett.getFeld(4,3).setBelegung(Feldbelegung.BLOCKIERT);
        
        
        
        Spielfenster fenster = new Spielfenster(spielbrett);
    }
}
